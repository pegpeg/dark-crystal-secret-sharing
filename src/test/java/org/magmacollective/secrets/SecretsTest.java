package org.magmacollective.secrets;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SecretsTest {

  @Test
  @DisplayName("Test encryption")
  public void testEncryption() {
    final String message = "beep boop";

    Secrets secret = new Secrets();
    final byte[] key = secret.generateSecretKey();
    final byte[] ciphertext = secret.encrypt(message.getBytes(), key);

    assertFalse(Arrays.equals(ciphertext, message.getBytes()));
    final Optional<byte[]> plaintext = secret.decrypt(ciphertext, key);

    assertTrue(plaintext.isPresent());
    String plaintextString = new String(plaintext.get(), StandardCharsets.UTF_8);
    assertTrue(plaintextString.equals(message));
  }
}