package org.magmacollective.secrets;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

public class SecretsMain {
  public static void main(String args[]) {
    Secrets secret = new Secrets();
    // secret.printName();

    final String message = "boop beep";
    final byte[] key = secret.generateSecretKey();
    final byte[] ciphertext = secret.encrypt(message.getBytes(), key);

    String outputString = new String(ciphertext, StandardCharsets.UTF_8);
    System.out.println("output: " + outputString);

    final Optional<byte[]> plaintext = secret.decrypt(ciphertext, key);

    if (plaintext.isPresent()) {
      String plaintextString = new String(plaintext.get(), StandardCharsets.UTF_8);
      System.out.println("Result of decryption: " + plaintextString);
    } else {
      System.out.println("Decryption Error");
    }
  }
}
