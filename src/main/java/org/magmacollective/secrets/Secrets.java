package org.magmacollective.secrets;

import java.io.*;
import java.util.Optional;
import java.security.SecureRandom;
import java.security.MessageDigest;
import org.spongycastle.crypto.engines.XSalsa20Engine;
import org.spongycastle.crypto.macs.Poly1305;
import org.spongycastle.crypto.params.ParametersWithIV;
import org.spongycastle.crypto.params.KeyParameter;

public class Secrets {
  private String name;

  // constuctor
  public Secrets() {
  }

  public static final byte KEY_BYTES = 32;
  public static final byte NONCE_BYTES = 24;
  public static final byte MAC_BYTES = 16;

  public static byte[] generateSecretKey() {
    final byte[] k = new byte[KEY_BYTES];
    final SecureRandom random = new SecureRandom();
    random.nextBytes(k);
    return k;
  }

  public static byte[] generateNonce() {
    final byte[] n = new byte[NONCE_BYTES];
    final SecureRandom random = new SecureRandom();
    random.nextBytes(n);
    return n;
  }

  public static byte[] encrypt(byte[] message, byte[] key) {
    final byte[] nonce = generateNonce();
    return byteArrayConcat(nonce, secretBox(message, key, nonce));
  }

  public static Optional<byte[]> decrypt(byte[] ciphertextWithNonce, byte[] key) {
    final byte[] nonce = new byte[NONCE_BYTES];
    System.arraycopy(ciphertextWithNonce, 0, nonce, 0, NONCE_BYTES);
    final byte[] ciphertext = new byte[ciphertextWithNonce.length - NONCE_BYTES];
    System.arraycopy(ciphertextWithNonce, NONCE_BYTES, ciphertext, 0, ciphertext.length);
    return secretUnbox(key, nonce, ciphertext);
  }

  public static byte[] secretBox(byte[] message, byte[] key, byte[] nonce) {
    final XSalsa20Engine xsalsa20 = new XSalsa20Engine();
    xsalsa20.init(true, new ParametersWithIV(new KeyParameter(key), nonce));

    // generate Poly1305 subkey
    final byte[] sk = new byte[KEY_BYTES];
    xsalsa20.processBytes(sk, 0, KEY_BYTES, sk, 0);

    final byte[] out = new byte[message.length + MAC_BYTES];
    xsalsa20.processBytes(message, 0, message.length, out, MAC_BYTES);

    // hash ciphertext and prepend mac to ciphertext
    final Poly1305 poly1305 = new Poly1305();
    poly1305.init(new KeyParameter(sk));
    poly1305.update(out, MAC_BYTES, message.length);
    poly1305.doFinal(out, 0);
    return out;
  }

  public static Optional<byte[]> secretUnbox(byte[] key, byte[] nonce, byte[] ciphertext) {
    final XSalsa20Engine xsalsa20 = new XSalsa20Engine();
    final Poly1305 poly1305 = new Poly1305();

    // initialize XSalsa20
    xsalsa20.init(false, new ParametersWithIV(new KeyParameter(key), nonce));

    // generate mac subkey
    final byte[] sk = new byte[KEY_BYTES];
    xsalsa20.processBytes(sk, 0, sk.length, sk, 0);

    // hash ciphertext
    poly1305.init(new KeyParameter(sk));
    final int len = Math.max(ciphertext.length - MAC_BYTES, 0);
    poly1305.update(ciphertext, MAC_BYTES, len);
    final byte[] calculatedMAC = new byte[MAC_BYTES];
    poly1305.doFinal(calculatedMAC, 0);

    // extract mac
    final byte[] presentedMAC = new byte[MAC_BYTES];
    System.arraycopy(ciphertext, 0, presentedMAC, 0, Math.min(ciphertext.length, MAC_BYTES));

    // compare macs
    if (!MessageDigest.isEqual(calculatedMAC, presentedMAC)) {
      return Optional.empty();
    }

    // decrypt ciphertext
    final byte[] plaintext = new byte[len];
    xsalsa20.processBytes(ciphertext, MAC_BYTES, plaintext.length, plaintext, 0);
    return Optional.of(plaintext);
  }

  public static byte[] byteArrayConcat(byte[] arr1, byte[] arr2) {
    byte[] result = new byte[arr1.length + arr2.length];
    System.arraycopy(arr1, 0, result, 0, arr1.length);
    System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
    return result;
  }
}
